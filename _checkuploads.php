<?php
@session_start();

ob_flush();
ob_start();

function _t($msg){
   print_r("</br>[".date("Y-m-d H:i:s")."] $msg |".PHP_EOL);
}
//_t("Starting the automation...");
$conn = null;
function connect(){
   try{
         $conn = new PDO('mysql:host=localhost;dbname=encbackups', 'root', 'elshan');
         $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         return $conn;
   }catch(PDOException $ex){
      _t($ex->getMessage(). " connection issue!");	
      die();
   }
}
//1
function copy_update_file($foldername,$filename){
   
   //copy_backup_file.sh e1725994612221db3808739b5465054e Job-2020-05-04-part1.backup
   //echo 'folder '.$foldername. ' file '.$filename;

   $result = shell_exec('sudo /var/www/html/scripts/copy_backup_file.sh '.$foldername.' '.$filename.' 2>&1');
   _t("1: ".$result);
}

/* ------4. Restore The Backup --------*/
function restore_backup($backup_filename){
   _t($backup_filename);
   $result = shell_exec('sudo /var/www/html/scripts/restore-backup.sh '.$backup_filename.' 2>&1');
   _t("4: " .$result);
}


/* ------3. Install old version --------*/
function install_old_version($version){
    $result = shell_exec('sudo /var/www/html/scripts/install-initial.sh '.$version.' 2>&1');
   _t('3: '.$result);
}

/* ------5. Install other new versions --------*/
function install_new_version($version){
   $result = shell_exec('sudo /var/www/html/scripts/install-new-versions.sh '.$version.' 2>&1');
   _t('4/5 : '.$result);   
}

function dohousekeeping(){
   $result = shell_exec('sudo /var/www/html/scripts/do-housekeeping.sh 2>&1');
   _t('6 : '.$result);   
}
function endsWith($string, $endStr)
{
    $length = strlen($endStr);
    if ($length == 0) {
        return true;
    }
    return (substr($string, -$length) === $endStr);
}
function startsWith ($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 
function take_new_backup($backup_filename,$fileuserid,$is_new = 0){
   _t('TAKING NEW BACKUP AS B TYPE: '.$backup_filename);
   $backup_filename = str_replace('backup','',$backup_filename);
   if(startsWith($backup_filename,"Job")){
      $result = shell_exec('sudo /var/www/html/scripts/take-new-backup.sh job '.$backup_filename.' '.$fileuserid.' '.$is_new.' 2>&1');
      _t('7 : '.$result); 
   }else if(startsWith($backup_filename,"Trend")){
      $result = shell_exec('sudo /var/www/html/scripts/take-new-backup_onthfly.sh encrypt '.$backup_filename.' '.$fileuserid.' '.$is_new.' 2>&1');
      _t('7 : '.$result); 
   }else if(startsWith($backup_filename,"Energy")){
      $result = shell_exec('sudo /var/www/html/scripts/take-new-backup_onthfly.sh encrypt '.$backup_filename.' '.$fileuserid.' '.$is_new.' 2>&1');
      _t('7 : '.$result); 
   }else if(startsWith($backup_filename,"Auto-Job")){
      $result = shell_exec('sudo /var/www/html/scripts/take-new-backup.sh job '.$backup_filename.' '.$fileuserid.' '.$is_new.' 2>&1');
      _t('7 : '.$result); 
   }else if(startsWith($backup_filename,"Auto-Trendlog")){
      $result = shell_exec('sudo /var/www/html/scripts/take-new-backup_onthfly.sh encrypt '.$backup_filename.' '.$fileuserid.' '.$is_new.' 2>&1');
      _t('7 : '.$result); 
   }else if(startsWith($backup_filename,"Auto-Energylog")){
      $result = shell_exec('sudo /var/www/html/scripts/take-new-backup_onthfly.sh encrypt '.$backup_filename.' '.$fileuserid.' '.$is_new.' 2>&1');
      _t('7 : '.$result); 
   }
   else{

   }
     
}


function __finish__($user_id){
   try{      
      $conx = connect();
      $stmt = "update files set fsendfirst=:first,finished_data=NOW() where id=:user_id";
      $pps = $conx->prepare($stmt);
      $pps->bindValue(':first', 1);
      $pps->bindValue(':user_id', $user_id);
      if ($pps->execute()) {
         $count = $pps->rowCount();
      }        
   } catch (PDOException $ex) {
      _t($ex->getMessage());
   } catch (Exception $ex) { 
      _t($ex->getMessage() ." finish fail with db issue");
   }
}

function sendEmailDone($emailAddr,$user_id){
   $to = $emailAddr;
   $subject = "Optergy Backup Conversion - Status [READY]";
   
   $message = "<p><b>Thank you for using the Optergy backup conversion tool.</b></p>";
   $message .= "<p>Your backups are ready to download. Pleaes use this url to download the backups.<a href='https://backuptool.optergy.com:8448/?p=download'>https://backuptool.optergy.com:8448/?p=download</a></p>";
   
   $header = "From:tools@optergy.com \r\n";
   $header .= "MIME-Version: 1.0\r\n";
   $header .= "Content-Type: text/html; charset=UTF-8\r\n"; 
   $retval = mail ($to,$subject,$message,$header); 
   __finish__($user_id); 
   if( $retval == true ) {
      _t("Notification email sent - ".$emailAddr);
   }else {
      _t("Something went wrong with the email function");
   }
}

function sendEmailDoneEx($emailAddr,$user_id,$backup_filename,$issue_code,$version){
   $to = $emailAddr;
   
   $message = "<p><b>Thank you for using the Optergy backup conversion tool.</b></p>";
   if($issue_code==1){
      $subject = "Optergy Backup Conversion - Status [READY]";   
      $message .= "<p>Your backup file [".$backup_filename."] is ready to download. Pleaes use this url to download the backups.<a href='https://backuptool.optergy.com:8448/?p=download'>https://backuptool.optergy.com:8448/?p=download</a></p>";
   }else if($issue_code==-4){
      $subject = "Optergy Backup Conversion - Status [ERROR]";
      $message .= "<p>Uploaded backup file[".$backup_filename."] is using version ".$version." and it is not compatable to convert into 3.0.2!";
      _t("Sending version issue email - ".$version);
   }
   $header = "From:tools@optergy.com \r\n";
   $header .= "MIME-Version: 1.0\r\n";
   $header .= "Content-type: text/html\r\n";   
   $retval = mail ($to,$subject,$message,$header); 
   //echo "email return";
   //var_dump($retval);
   if( $retval == true ) {
      _t("Notification email sent - ".$emailAddr);
   }else {
      _t("Something went wrong with the email function");
   }
}

function __finish__ex($emailAddr,$user_id,$backup_filename,$issue_code=1,$version=""){
   try{      
      _t("Calling finish and sending an email with -".$issue_code."-".$user_id."-".$version);
      $conx = connect();
      $stmt = "update files set fsendfirst=:first,finished_data=NOW(),version=:version where id=:user_id and fsendfirst=0";
      $pps = $conx->prepare($stmt);
      
      $pps->bindValue(':first', $issue_code);
      $pps->bindValue(':user_id', $user_id);
      $pps->bindValue(':version', $version);
      $result = $pps->execute();
      //var_dump($result);
      if(intval($result)>0){
        // $count = $pps->rowCount();
        // if($count>0){
            _t("Update the db with finish and try to send the email");
            sendEmailDoneEx($emailAddr,$user_id,$backup_filename,$issue_code,$version);
            
        // }
      }        
   } catch (PDOException $ex) {
      _t($ex->getMessage());
   } catch (Exception $ex) { 
      _t($ex->getMessage() ." finish fail with db issue");
   }
}

function checkInitialInstallVersions($email,$version,$backup_filename,$fileuserid,$userid){
   $version = (string)(trim($version));
   _t($version);
   switch($version){
      case '3.0.0':
         install_old_version("proton-2.0.6a.deb"); //first install this then next
         install_new_version("proton-3.0.0.deb");
         restore_backup($backup_filename);
         install_new_version("proton-3.0.2a.deb");
         dohousekeeping();
         take_new_backup($backup_filename,$fileuserid,1);
         //sendEmailDone($email,$userid);
         __finish__ex($email,$userid,$backup_filename,"1",$version);
      break;
      case '2.4.12':
         install_old_version("proton-2.0.6a.deb");
         install_new_version("proton-2.4.12.deb");
         restore_backup($backup_filename);
         install_new_version("proton-3.0.0.deb");
         install_new_version("proton-3.0.2a.deb");
         dohousekeeping();
         take_new_backup($backup_filename,$fileuserid);
         //sendEmailDone($email,$userid);
         __finish__ex($email,$userid,$backup_filename,"1",$version);
      break;
      case '2.4.10':
         install_old_version("proton-2.0.6a.deb");         
         install_new_version("proton-2.4.10.deb");
         restore_backup($backup_filename);
         install_new_version("proton-3.0.0.deb");
         install_new_version("proton-3.0.2a.deb");
         dohousekeeping();
         take_new_backup($backup_filename,$fileuserid);
         //sendEmailDone($email,$userid);
         __finish__ex($email,$userid,$backup_filename,"1",$version);
      break;
      case '2.4.9':
         install_old_version("proton-2.0.6a.deb");
         install_new_version("proton-2.4.9.deb");
         restore_backup($backup_filename);
         install_new_version("proton-3.0.0.deb");
         install_new_version("proton-3.0.2a.deb");
         dohousekeeping();
         take_new_backup($backup_filename,$fileuserid);
         //sendEmailDone($email,$userid);
         __finish__ex($email,$userid,$backup_filename,"1",$version);
      break;
      case '2.4.6':
         install_old_version("proton-2.0.6a.deb");
         install_new_version("proton-2.4.6.deb");
         restore_backup($backup_filename);
         install_new_version("proton-3.0.0.deb");
         install_new_version("proton-3.0.2a.deb");
         dohousekeeping();
         take_new_backup($backup_filename,$fileuserid);
         //sendEmailDone($email,$userid);
         __finish__ex($email,$userid,$backup_filename,"1",$version);
      break;
      case '2.0.6a':
         print 'need to install 2.0.6a';
         install_old_version("proton-2.0.6a.deb");
         restore_backup($backup_filename);
         install_new_version("proton-3.0.0.deb");
         install_new_version("proton-3.0.2a.deb");
         dohousekeeping();
         take_new_backup($backup_filename,$fileuserid);
         //sendEmailDone($email,$userid);
         __finish__ex($email,$userid,$backup_filename,"1",$version);
      break;
      default:
         print "System didn't support this version [".$version."]";
         __finish__ex($email,$userid,$backup_filename,-4,$version);
   }
}
function startAutomation($emailAddr,$userid,$filename,$fileuserid){

   $filepath = '/var/www/html/jfileupload/server/php/files/'.$fileuserid."/$filename";
    /* ------1.copy file --------*/
    copy_update_file($fileuserid,$filename);
   
   /* ------1. check version --------*/
  $result = shell_exec('sudo /var/www/html/scripts/version_check.sh '.$filename.' 2>&1');
  _t("2:".$result);
  
   //3. install deb files and restore the backup
   checkInitialInstallVersions($emailAddr,$result,$filename,$fileuserid,$userid);

}

function updateFileTableAfterEmail($emailAddr,$user_id,$filename,$fileuserid){
   try{      
      $conn = connect();
      $stmt = "update files set fsendfirst=:first where id=:user_id";
      $pps = $conn->prepare($stmt);
      $pps->bindValue(':first', 0);
      $pps->bindValue(':user_id', $user_id);
      if ($pps->execute()) {
         $count = $pps->rowCount();
         //if($count>0)
         startAutomation($emailAddr,$user_id,$filename,$fileuserid);
      }        
   } catch (PDOException $ex) {
      _t($ex->getMessage());
   } catch (Exception $ex) { 
      _t($ex->getMessage() ." updateFileTableAfterEmail");
   }
}


function sendEmail($emailAddr,$id,$filename,$fileuserid){
   $to = $emailAddr;
   $subject = "Optergy Backup Conversion - Status [Processing]";
   
   $message = "<p><b>Thank you for using the Optergy backup conversion tool.</b></p>";
   $message .= "<p>We are converting your backup file [".$filename."]. We will send another email with the link to download your converted proton backup.</p>";
   
   $header = "From:tools@optergy.com \r\n";
   $header .= "MIME-Version: 1.0\r\n";
   $header .= "Content-type: text/html\r\n";
   
   $retval = mail ($to,$subject,$message,$header);
   
   if( $retval == true ) {
      _t("Notification email sent - ".$emailAddr);
      updateFileTableAfterEmail($emailAddr,$id,$filename,$fileuserid);
   }else {
      _t("Something went wrong with the email function");
   }
}
//set 0 if we are going to use the system.
//then set 1 when finished
//check 0 in any column to stop the time taking.
function checkIsRunning(){
   $stmt = "select id from files where fsendfirst=0 limit 0,1";
   try{
      $conx = connect();
      $result = $conx->prepare($stmt);
      if ($result->execute()) {
         $row = $result->fetch(PDO::FETCH_ASSOC);
         if($row){
            return true;
         }else{
            return false;
         }
      }
      return true;
   }catch(Exception $ex){
      return true;
   }
}

function __init__(){
   try {
      $stmt_for_execute = "select files.id,users.name,files.userid,filename from files inner join users on users.loginkey=files.userid where files.fsendfirst is NULL order by files.uploaded_date limit 0,1;";
      $conx = connect();
      $result = $conx->prepare($stmt_for_execute);
      if ($result->execute()) {
         $row = $result->fetch(PDO::FETCH_ASSOC);
         if($row){
            sendEmail($row['name'],$row['id'],$row['filename'],$row['userid']);
         }else{
            _t("no uploaded files to process..");
         }            
      }
   } catch (Exception $ex) {
      _t($ex->getMessage() ." : trying to send an email");
   }
}

if(!checkIsRunning()){
   __init__();
}else{
   _t("System already processing a file...");
}
try{
   $myfile = file_put_contents('/var/www/html/logs.txt', ob_get_flush().PHP_EOL , FILE_APPEND | LOCK_EX);
}catch(Exception $ex){
   echo 'cannot write the logs...';
}

?>