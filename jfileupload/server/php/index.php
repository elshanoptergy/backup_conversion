<?php
/*
 * jQuery File Upload Plugin PHP Example
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */
@session_start();
require('UploadHandler.php');
//$options = null, $initialize = true, $error_messages = null,$user_id='0'
if(isset($_SESSION['logged']) && isset($_SESSION['loginkey'])){
    $id = stripslashes($_SESSION['loginkey']);
    $upload_handler = new UploadHandler($id,null,true,null);
}
