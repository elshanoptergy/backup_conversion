/*
 * jQuery File Upload Demo
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* global $ */

$(function () {
    'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        autoUpload:false,
        xhrFields: {withCredentials: true},
        url: 'server/php/',
        dataType:'json'
    });
    
    $('#fileupload').fileupload('option', {
        maxFileSize:999000*1000,
        acceptFileTypes: /(\.|\/)(backup|gpg)$/i
    });

    $('#fileupload').addClass('fileupload-processing');
    $.ajax({
        url: $('#fileupload').fileupload('option', 'url'),
        dataType: 'json',
        context: $('#fileupload')[0]
    }).always(function () {
        $(this).removeClass('fileupload-processing');
    }).done(function (result) {
            $(this).fileupload('option', 'done').call(this, $.Event('done'), {result: result});           
    });
    $('#fileupload')
    .on('fileuploadadd',function(e,data){

        $.each(data.files, function (index, file) {
            console.log('Added file: ' + file.name);
            if( file.name.indexOf('Job') == 0 ||
                file.name.indexOf('Trend') == 0 ||
                file.name.indexOf('Energy') == 0||
                file.name.indexOf('Auto-Job') == 0 ||
                file.name.indexOf('Auto-Energy') == 0 ||
                file.name.indexOf('Auto-Trend') == 0 ){

            }else{
                var dfd = $.Deferred()
                var file_ = file;
                file_.error = 'Invalid file type.';
                dfd.rejectWith(this, [data]);
                //data.abort();
                return dfd.promise();
            }            
        });
    });
   
});
