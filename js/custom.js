const _URL = "/864API";
const alert_types = {
    warning: 'warning',
    error: 'error',
    info: 'info'
}

function log(data) {
    //console.log(data);
    for (var i = 0; i < data.length; i++) {
        var objects = data.length[i];
        console.log(objects);
        for (var r = 0; r < objects.length; r++) {
            console.log(objects[r]['id']);
        }
    }
}
function ajax(data, method, api) {
    var payload;
    if (data !== null) {
        payload = data;
    }
    return new Promise((resolve, reject) => {
        $.ajax({
            type: method,
            url: _URL + api,
            data: JSON.stringify(payload),
            contentType: "application/json; charset=utf-8",
            crossDomain: true,
            dataType: "json",
            success: function (data, status, jqXHR) {
                resolve(data);
            },
            error: function (jqXHR, status) {
                reject(status);
            }
        });
    });
}
function uploadMacs() {
    var file_data = $("#validatedCustomFile").prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    console.log(form_data);
    if (form_data) {
        $.ajax({
            type: 'POST',
            url: "./include/inc/mac-uploads.php",
            data: form_data,
            crossDomain: true,
            success: function (data) {
                console.log(data);
            }
        });
    }
}
function insertMacs() {
    var rig_id = $("#customeTestRig").val();
    var numof_macs = $("#macAddresses").val();
    if (!isNaN(rig_id) && !isNaN(numof_macs)) {
        ajax(null, "POST", "/macrigs/add/?testrig_id=" + rig_id + "&mac_address_id=" + numof_macs).then(data => {
            showAlertDialog('MAC address successfully assigned!', alert_types.info);
        }).catch(error => {
            showAlertDialog(error, alert_types.error);
        });
    } else {
        showAlertDialog("Invalid integer value on MAC address feild.", alert_types.error);
    }
}
function showAlertDialog(message, alert_type) {
    let type = alert_type;
    var div_msg = '<div class="em_messagebox';
    if (message) {
        switch (type) {
            case alert_types.error:
                type = "alert-danger";
                break;
            case alert_types.info:
                type = "alert-info";
                break;
            case alert_types.warning:
                type = "alert-warning";
                break;
            default:
                type = "alert-success";
        }
        div_msg +=' alert ' + type + ' alert-dismissible fade show"';
        div_msg +='role="alert">';
        div_msg +='<span id="em_messagetxt">'+message+'</span>';
        div_msg +='<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        div_msg +='</div>';
        $('.alertbox').html("");
        $('.alertbox').append(div_msg);
        return true;
    } else {
        return false;
    }
}
function manipulateUsers(object){
        var key = ($(object)).attr("data");
        var stat = ($(object)).attr("nodetype");
        $.ajax({
            type: 'POST',
            url: './include/inc/rigs-manipulation.php',
            data:{useractivate:key,status : stat}
        }).done(function(data){
            if(data==1){
                if(stat==0){
                        $(object).text('Activate');
                        $(object).attr("nodetype",1);
                        $(object).attr("class","btn btn-primary");

                    }else{
                        $(object).text('Deactivate');
                        $(object).attr("nodetype",0);
                        $(object).attr("class","btn btn-danger");
                 }
            }
        });
};
/*I know there's short hand method but I love this $(document)*/
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('.editrig').click(function(e){
        //e.preventExtensions()
        var id = $(this).attr('data');
        $.ajax({
            type:'POST',
            url:'./include/inc/rigs-manipulation.php',
            data:{name:'rigid',params:id},
        }).done(function(data){
            if(data.hasOwnProperty('result')){
                $("#testrigIdAd").val(data.result.rig_code);
                $("#testrigIpAd").val(data.result.ipaddr);
                $("#testrigDesAd").val(data.result.description);
                $("#testrigid_ex").val(data.result.id);
                $("#btnsubmitad").text("Update");
                window.location.href="#toppos";
            }
        });
    });
    $('#loginform').submit(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:"./include/inc/rigs-manipulation.php",
            data:$('form#loginform').serialize()
        }).done(function(msg){
            if(msg.hasOwnProperty('activated') && msg.hasOwnProperty('logged')){
               if(msg.activated ==false){
                showAlertDialog("Account is not activated. please check back later!", alert_types.error);
               }else if(msg.logged ==false){
                showAlertDialog("Invalid username or password. Try again.", alert_types.error);
               }else if(msg.activated == true && msg.logged ==true){
                showAlertDialog("Success!", alert_types.info);
                setTimeout(function(){ window.location.href="/"; }, 500);
               }
            }else{
                showAlertDialog("Invalid username or password", alert_types.error);
            }
        });
    })
     $('#registerform').submit(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:"./include/inc/rigs-manipulation.php",
            data:$('form#registerform').serialize()
        }).done(function(msg){
            console.log(msg);
            if(msg.hasOwnProperty('register')){
                showAlertDialog(msg.register, alert_types.error);
            }
            if(msg.hasOwnProperty('show')){
                if(msg.show == true){
                    $('#registerModel').modal('show');
                }
            }
        });
    });
      $('#resetpassword').submit(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:"./include/inc/rigs-manipulation.php",
            data:$('form#resetpassword').serialize()
        }).done(function(msg){
            if(msg.hasOwnProperty('register')){
                showAlertDialog(msg.register, alert_types.info);
            }
            if(msg.hasOwnProperty('register')){
                if(msg.register.indexOf("instruction")!== -1){
                    setTimeout(function(){ window.location.href="/"; }, 3000);
                }
            }
        });
    });

    $('#resetpassword-withid').submit(function(e){
        e.preventDefault();
        $.ajax({
            type:'POST',
            url:"./include/inc/rigs-manipulation.php",
            data:$('form#resetpassword-withid').serialize()
        }).done(function(msg){
            if(msg.hasOwnProperty('register')){
                showAlertDialog(msg.register, alert_types.info);
            }
            if(msg.hasOwnProperty('register')){
                if(msg.register.indexOf("changed")!== -1){                    
                    setTimeout(function(){ window.location.href="/"; }, 3000);
                }
            }
        });
    });
    
    $("button#register").click(function(){
        location.href="/?p=register";
    });

    $("#convert-all-backups").click(function(e){
        e.preventDefault();
        $.ajax({
            type:'GET',
            url:"./include/inc/rigs-manipulation.php?start"
        }).done(function(msg){
            console.log(msg.process);
            $('.embed-responsive-item').css('opacity','0.5').css('pointer-events','none');
        });
    });
    $(".delete_converted").on('click', function(event) {
        event.preventDefault();
        $object = $(this);
        $filename = $(this).attr('data');
        $.ajax({
            type:'GET',
            url:"./include/inc/rigs-manipulation.php?delete_file="+$filename
        }).done(function(msg){
            if(msg.data==true){
                $object.closest("tr").hide();
            }
        });
    });
    $(".delete_non_related_info").on('click', function(event) {
        event.preventDefault();
        $object = $(this);
        $filename = $(this).attr('data');
        $.ajax({
            type:'GET',
            url:"./include/inc/rigs-manipulation.php?delete_info="+$filename
        }).done(function(msg){
            if(msg.data==true){
                $object.closest("tr").hide();
            }
        });
    });
    
    $(function () {
        $('[data-toggle="tooltip"]').tooltip();
    });

    $("span#basic-addon1").on('click', function(event) {
        event.preventDefault();
        if($('#showhidepassword_').attr("type") == "text"){
            $('#showhidepassword_').attr('type', 'password');
            $('span#basic-addon1 i').addClass( "fa-eye-slash" );
            $('span#basic-addon1 i').removeClass( "fa-eye" );
        }else if($('#showhidepassword_').attr("type") == "password"){
            $('#showhidepassword_').attr('type', 'text');
            $('span#basic-addon1 i').removeClass( "fa-eye-slash" );
            $('span#basic-addon1 i').addClass( "fa-eye" );
        }
    });
    
});
