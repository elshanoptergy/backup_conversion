
<?php

@session_start();
$ROOT_URL = $_SERVER["DOCUMENT_ROOT"];
include_once $ROOT_URL . '/Controllers/DBManipulation.php';
?>
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="./">
        <div class="sidebar-brand-icon">
            <img src="/img/optergy-logo.png" height="42" width="42" alt="Convert Backup | By Optergy">
        </div>
        <div class="sidebar-brand-text mx-3">Convert Backup</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="./">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Home</span></a>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">Convert Backup</div>
    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="./?p=upload" >
            <i class="fas fa-fw fa-upload"></i>
            <span>Upload Backups</span>
        </a>
        <a class="nav-link collapsed" href="./?p=download" >
            <i class="fas fa-fw fa-download"></i>
            <span>Download Backups</span>
        </a>
    </li>
    <?php 
        $approved_users = false;
        $util = new DBManipulation();
        $approved_users = $util->isValidAdmin();
        if($approved_users){
    ?>
    <hr class="sidebar-divider">
    <!-- Heading -->
    <div class="sidebar-heading">User Management</div>
    <li class="nav-item">
        <a class="nav-link collapsed" href="./?p=approve" >
            <i class="fas fa-fw fa-users"></i>
            <span>Clients</span>
        </a>
    </li>
    <?php
        }
    ?>
        <!--
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Register User</h6>
                <a class="collapse-item" href="./?p=client">Register</a>
            </div>
        </div>
-->
    
    
</ul>
<div id="content-wrapper" class="d-flex flex-column">
    <!-- Main Content -->
    <div id="content">
        <!-- Topbar -->
        <?php include_once 'topnav.php'; ?>