<?php 
$url = "./jfileupload/index.php";
$_undermaintain = FALSE;
  if($_undermaintain){
    if(isset($_SESSION['logged'])){
      if($_SESSION['logged_user']=='elshan0011@gmail.com'){
        $url = "./jfileupload/index___.php";
    }
  }else{
    //$url = "./jfileupload/index___.php";
  }
}
?>
<div class="container-fluid">
    <h3 class="h5 mb-4 text-gray-800"></h3>
    <div class="card o-hidden border-0 shadow-lg my-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Encrypt Backups</h6><h6>Supported versions : Proton 2.0.6a/2.4.6/2.4.9/2.4.10/2.4.12/3.0.0</h6>
            <h6 class="m-0 font-weight-bold">We are scheduling your backups as soon as you upload the backup files.. We will send an email as soon as it is complete.</h6>
            <p>&nbsp;</p>
            <h5 class="m-0 font-weight-bold text-danger"> Note:</h5>
            <h6 class="m-0 ">Every backup file starts from the <strong class="text-primary">Job</strong> or <strong class="text-primary">Trend</strong> or <strong class="text-primary">Energy</strong> or <strong class="text-primary">Auto-Job</strong> or <strong class="text-primary">Auto-Trend</strong> or <strong class="text-primary">Auto-Energy</strong>.</h6>
            
            <!--<button id="convert-all-backups"  style="float:right" class="btn btn-primary" data-target="#popupconversion" data-toggle="modal"><i class="fas fa-fw fa-save"></i> Convert Backups</button>-->
        </div>
        <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="embed-responsive embed-responsive-1by1">
                        <iframe class="embed-responsive-item" src="<?php echo $url; ?>"></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="popupconversion" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Backup Conversion Tool</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Backup conversion has been started. We will send an email along with the backup download link.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div>
  </div>
</div>