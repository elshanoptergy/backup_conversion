<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="robots" content="noindex,nofollow">
        <meta name="description" content=" Backup Conversion Tool">
        <meta name="author" content=" Optergy | By Elshan">
        <link rel="icon" href="/img/Optergy_Favicon-300x290.png" type="image/png"><link rel="icon" href="/img/cropped-Optergy-Favicon-512x512-32x32.png" sizes="32x32" /><link rel="icon" href="/img/cropped-Optergy-Favicon-512x512-192x192.png" sizes="192x192" />
        <title><?php echo $OPTERGY['page']['title']; ?></title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.css" rel="stylesheet">
        <link href="css/default.css" rel="stylesheet">
        <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
        <style>
            body{
                background-image: url('/img/background_.jpg');
                background-size: cover;
            }
        </style>
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <div class="container">
                <form class="form-signin" id="registerform">
                    <div class="img-container">
                        <img class="mb-5" src="/img/Optergy_Logo-1-300x77.png" alt="Optergy 864 Test Rig Login" id="loginlogo" >
                    </div>
                    <div class="mb-5 alertbox"></div>
                    <div class="form-group">
                        <h6 class="label-primary">Register</h6>
                    </div>
                    <div class="form-group">
                        <label for="username" class="sr-only">Email</label>
                        <input type="text" id="username" name="registeremail" class="form-control" placeholder="Email" required=""  autocomplete="new-email" autofocus="" />
                    </div>
                    <!-- <div class="form-group">
                        <label for="password" class="sr-only">Password</label>
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="" minlength="6"  autocomplete="new-password" />
                    </div> -->
                     
                    <div class="input-group mb-3" id="show_hide_password">
                        <label for="showhidepassword_" class="sr-only">Password</label>
                      <input type="password" id="showhidepassword_" name="password" class="form-control" placeholder="Password" required="" minlength="6"  autocomplete="new-password" aria-label="password" aria-describedby="basic-addon1">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fa fa-eye-slash" aria-hidden="true"></i></span>
                      </div>
                    </div>
                    <div class="row">
                        <div class="col-sm">
                            <button class="btn btn-primary btn-block"  type="submit">Register</button>
                        </div>
                    </div>
                    <div class="mb-5"><hr/></div>
                    <div class="row forgotpw">
                        <div class="col-sm">
                            <label class="label label-primary">Already registered? <a href="/" title="Login">Log In</a></label>
                        </div>
                    </div>
                </form>
            </div>
            <!-- End of Footer -->
        </div>
        <div class="modal fade" id="registerModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">User Registation Success</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">We will review your account and send an email when we finish the review process!</div>
                    <div class="modal-footer">
                        <a class="btn btn-primary" href="index.php">OK</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright text-center my-auto logincopyrights">
            <span>Copyright &copy;<?php echo date("Y"); ?> Optergy - Backup Conversion v0.2</span>
        </div>
        <!-- Bootstrap core JavaScript-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- Core plugin JavaScript-->
        <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
        <!-- Custom scripts for all pages-->
        <script src="js/sb-admin-2.min.js"></script>
        <!-- Page level plugins -->
        <script src="vendor/datatables/jquery.dataTables.min.js"></script>
        <script src="vendor/datatables/dataTables.bootstrap4.min.js"></script>

        <!-- Page level custom scripts -->
        <script src="js/demo/datatables-demo.js"></script>
        <script src="js/custom.js"></script>
    </body>
</html>
