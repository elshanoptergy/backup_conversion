<?php
    @session_start();
    include_once './Controllers/DBManipulation.php';

    $dbManip = new DBManipulation();
    $user_key = isset($_SESSION['loginkey']) ? $_SESSION['loginkey'] : 0;
    $ROOT_URL = $_SERVER["DOCUMENT_ROOT"];
    $FILES = 0;
    $fulldir = '';
    
?>
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">View encrypted files</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr><th>#</th>
                            <th>File Name</th>
                            <th>Version</th>
                            <th data-toggle="tooltip" data-placement="top" title="N/C - Not converted">Status</th>
                            <th>Uploaded Date</th>
                            <th>Backup Converted Date</th>
                            <th>Download Link</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($user_key) && !empty($user_key)){
                            $dbManip->getUploadedBackups($user_key);
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
