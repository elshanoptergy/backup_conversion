<?php
    @session_start();
    include_once './Controllers/DBManipulation.php';

    $dbManip = new DBManipulation();
    $user_key = isset($_SESSION['loginkey']) ? $_SESSION['loginkey'] : 0;
    $ROOT_URL = $_SERVER["DOCUMENT_ROOT"];
    $FILES = 0;
    $fulldir = '';
    
?>
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">User Management</h6>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User Name</th>
                            <th>Backup Dir</th>
                            <th>Created Date</th>
                            <th>PW Changed</th>
                            <th>Option</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if(isset($user_key) && !empty($user_key)){
                            $dbManip->getUserStatus();
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
