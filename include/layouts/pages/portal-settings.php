<?php
include_once './Controllers/DBManipulation.php';
include_once './Controllers/Helpers.php';
$dbManip = new DBManipulation();
$rig_id = Helpers::ReadRigCode();
?>
<div class="container-fluid">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Settings</h6>
        </div>
        <div class="card-body" p-0>
            <div class="row">
                <div class="col-lg-7">
                    <div class="p-5">
                        <form id="insertRigCode">
                            <div class="form-group row">
                                <label for="customeTestRig" class="col-sm-4 col-form-label">Test RIG Code</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="testrigId" value="<?php echo $rig_id; ?>"/>
                                </div>
                                <label class="col-sm-4 col-form-label" id="numofmacs"></label>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-primary">Change</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>