<?php
include_once './Controllers/DBManipulation.php';
include_once './Controllers/Helpers.php';
$dbManip = new DBManipulation();
?>
<div class="container-fluid" id="toppos">
    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">RIGs Manipulation</h6>
        </div>
        <div class="card-body" p-0>
            <div class="row">
                <div class="col-lg-7">
                    <div class="p-5">
                        <form id="insertRigsAd">
                            <div class="form-group row">
                                <label for="customeTestRig" class="col-sm-4 col-form-label">Hardware ID</label>
                                <div class="col-sm-4">
                                    <input type="hidden" name="testrigid" id="testrigid_ex" />
                                    <input type="text" name="testrigidad" class="form-control" id="testrigIdAd" value="" maxlength="124"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="customeTestRig" class="col-sm-4 col-form-label">Description</label>
                                <div class="col-sm-4">
                                    <input type="text" name="testrigdescad" class="form-control" id="testrigDesAd" value="" maxlength="220"/>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">&nbsp;</div>
                                <div class="col-sm-4">
                                    <button type="submit" class="btn btn-primary" id="btnsubmitad">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>RIG ID</th>
                                    <th>Hardware ID</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>RIG ID</th>
                                    <th>Hardware ID</th>
                                    <th>Description</th>

                                </tr>
                            </tfoot>
                            <tbody>
                                <?php
                                $dbManip->getAllTestRigsForEdit();
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>