<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="robots" content="noindex,nofollow">
        <meta name="description" content=" Backup Conversion Tool">
        <meta name="author" content=" Optergy | By Elshan">
        <link rel="icon" href="/img/Optergy_Favicon-300x290.png" type="image/png">
        <link rel="icon" href="/img/cropped-Optergy-Favicon-512x512-32x32.png" sizes="32x32" />
        <link rel="icon" href="/img/cropped-Optergy-Favicon-512x512-192x192.png" sizes="192x192" />
        <title><?php echo $OPTERGY['page']['title']; ?></title>
        <!-- Custom fonts for this template-->
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <!-- Custom styles for this template-->
        
        <link href="css/sb-admin-2.css" rel="stylesheet">
        <link href="css/default.css" rel="stylesheet">
        <link href="vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    </head>
    <body id="page-top">
        <!-- Page Wrapper -->
        <div id="wrapper">
            <?php include_once 'menu.php'; ?>