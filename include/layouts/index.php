<?php

if (session_status() == PHP_SESSION_NONE) {
    @session_start();
}
$ROOT_URL = $_SERVER["DOCUMENT_ROOT"];
include_once $ROOT_URL . '/Controllers/DBManipulation.php';
$checkUploads = new DBManipulation();
$has_uploads = $checkUploads->checkUserHasUploads();
$page = '';

if (isset($_GET['p'])) {
    $page = $_GET['p'];
}

if (!isset($_SESSION['logged']) && empty($_SESSION['logged']) ) {
    switch ($page) {
        case 'register':
            include_once($ROOT_URL . '/include/layouts/pages/register.php');
            break;
        case 'forgot':
            include_once($ROOT_URL . '/include/layouts/pages/forgot-password.php');
            break;
        case 'resetpassword':
            include_once($ROOT_URL . '/include/layouts/pages/reset-password.php');
            break;    
        default:
            include_once($ROOT_URL . '/include/layouts/pages/login.php');
            break;
    }
} else {
    include_once 'head.php';
    switch ($page) {
        case 'dashboard':
            include_once($ROOT_URL . '/include/layouts/pages/upload.php');
            break;
        case 'login':
            include_once($ROOT_URL . '/include/layouts/pages/login.php');
            break;
        case 'upload':
            include_once($ROOT_URL . '/include/layouts/pages/upload.php');
            break;
        case 'download':
            include_once($ROOT_URL . '/include/layouts/pages/view-results.php');
            break;
        case 'client':
            include_once($ROOT_URL . '/include/layouts/pages/add-client.php');
            break;
        case 'approve':
            include_once($ROOT_URL . '/include/layouts/pages/approve-users.php');
            break;
        default :
            if($has_uploads){
                include_once($ROOT_URL . '/include/layouts/pages/view-results.php');
            }else{
                include_once($ROOT_URL . '/include/layouts/pages/upload.php');
            }
            break;
    }
    include_once 'footer.php';
}
?>