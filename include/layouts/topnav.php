<?php
$username = "";
if (isset($_SESSION['logged_user']) && !empty($_SESSION['logged_user'])) {
    $username = stripslashes($_SESSION['logged_user']);
} else {
    $username = "Guest";
}
?>
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
    <!-- Sidebar Toggle (Topbar) -->
    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
        <i class="fa fa-bars"></i>
    </button>
    <!-- Topbar Navbar -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown no-arrow mx-1">
              <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-question-circle" data-toggle="tooltip" data-placement="top" title="Looking for help?"></i>
              </a>
              <!-- Dropdown - Alerts -->
            <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
                <h6 class="dropdown-header">
                  Help Center
                </h6>
                <a class="dropdown-item d-flex align-items-center" target="_blank" href="https://buy.optergy.com/helpdesk/customer/">
                  <div class="mr-3">
                    <div class="icon-circle bg-primary">
                      <i class="fas fa-user-friends text-white"></i>
                    </div>
                  </div>
                  <div>
                    <div class="small text-gray-500">Looking for help?</div>
                    <span class="font-weight-bold">Optergy Help Center</span>
                  </div>
                </a>
            </div>
        </li>
        <div class="topbar-divider d-none d-sm-block"></div>
        <!-- Nav Item - User Information -->
        <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small" data-toggle="tooltip" data-placement="top" title="Click here for logout..."><?php echo ucfirst($username); ?></span>
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                    Logout
                </a>
            </div>
        </li>
    </ul>
</nav>