<?php
@session_start();

$ROOT_URL = $_SERVER["DOCUMENT_ROOT"];
include_once $ROOT_URL . '/Controllers/DBManipulation.php';
$result = array();
$errors = array();

if (isset($_REQUEST['name']) && !empty($_REQUEST['name'])) {
    if (isset($_REQUEST['params']) && !empty($_REQUEST['params'])) {
        $testRigs = new DBManipulation();
        $data['result'] = $testRigs->getRigInfoFromId($_REQUEST['params']);
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}
// user login
if(isset($_REQUEST['username']) && isset($_REQUEST['password'])){
    if(!empty($_REQUEST['username']) && !empty($_REQUEST['password'])){
        $login = new DBManipulation();
        $username = strval(trim($_REQUEST['username']));
        $password = strval($_REQUEST['password']);
        $logged = $login->checkLogin($username,$password);
        header('Content-Type: application/json');
        if(is_array($logged)){
            if(isset($logged['factive']) && !empty($logged['factive'])){
                if(intval($logged['factive'])==1){
                    $_SESSION['logged_level']=$logged['level'];
                    $_SESSION['logged_user']=$logged['name'];
                    $_SESSION['logged']=TRUE;
                    $_SESSION['loginkey']=$logged['loginkey'];
                    echo json_encode(['logged'=>TRUE,'activated'=>TRUE]);//sucess 
                }else{
                    echo json_encode(['logged'=>TRUE,'activated'=>FALSE]); //not activated
                    $_SESSION['loginkey']=0;
                }
            }else{
                echo json_encode(['logged'=>TRUE,'activated'=>FALSE]); //not activated
                $_SESSION['loginkey']=0;
            }
        }else{
            echo json_encode(['logged'=>FALSE,'activated'=>FALSE]);
            $_SESSION['loginkey']=0; //invalid username
        }
    }
}
function endsWith($str,$guard){   
    return (substr($str, -strlen($guard))===$guard);
}
function checkUsers($username){
    $emails = array("@optergy.com","@alerton.com.au","lshan0011@gmail.com");
    for($i=0;$i<count($emails);$i++){
        if(endsWith($username,$emails[$i])){
           return true;
        }
    }
return false;
}
//password reset informaion
function sendEmailForPasswordReset($user_id,$email){
   $to = $email;
   $subject = "Optergy Backup Conversion - Reset Password";
   
   $message = "<p><b>Please click below link to reset the password.</b></p>";
   $message .= "<p>https://backuptool.optergy.com:8448/?p=resetpassword&user=".$user_id."&email=".$email."</p>";
   $header = "From:tools@optergy.com \r\n";
   $header .= "MIME-Version: 1.0\r\n";
   $header .= "Content-type: text/html\r\n";   
   $retval = mail ($to,$subject,$message,$header);   
   return $retval;
}
function sendUserRegistationEmail($user_id,$email){
   $to = $email;
   $subject = "Optergy Backup Conversion - Reset Password";
   
   $message = "<p><b>Please click below link to reset the password.</b></p>";
   $message .= "<p>https://backuptool.optergy.com:8448/?p=resetpassword&user=".$user_id."&email=".$email."</p>";
   $header = "From:tools@optergy.com \r\n";
   $header .= "MIME-Version: 1.0\r\n";
   $header .= "Content-type: text/html\r\n";   
   $retval = mail ($to,$subject,$message,$header);   
   return $retval;
}
function sendUserNeedsToActivateEmail($username){
    $getAdminusers = new DBManipulation();
    $to = $getAdminusers->getValidAdminEmails();
    //var_dump($to);
    $toaddress = array();
    if(isset($to) && !empty($to)){
        foreach($to as $row) {
            $toaddress[] = $row['name'];
        }
    }
    
    $subject = "Optergy Backup Conversion - New User Registered";
    $message = "<p><b>New user [".$username."] added to the system.</b></p>";
    $message .= "<p>Please log in to the system to activate that account https://backuptool.optergy.com:8448/?p=approve</p>";
    $header = "From:tools@optergy.com \r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-type: text/html\r\n";
    if(!empty($toaddress) && isset($toaddress)){
        foreach ($toaddress as $key => $emailadr) {
            mail ($emailadr,$subject,$message,$header); 
        }
        //$retval = mail ($to,$subject,$message,$header);   
    }
}

//password reset
if( (isset($_REQUEST['rest-emailaddress']) && !empty($_REQUEST['rest-emailaddress'])) &&
    (isset($_REQUEST['rest-password']) && !empty($_REQUEST['rest-password'])) && 
    (isset($_REQUEST['rest-retypepassword']) && !empty($_REQUEST['rest-retypepassword'])) && 
    (isset($_REQUEST['rest-hiddenid']) && !empty($_REQUEST['rest-hiddenid'])) ){

     $email = $_REQUEST['rest-emailaddress'];
     $password = $_REQUEST['rest-password'];
     $hiddenid = $_REQUEST['rest-hiddenid'];
     $password_2 = $_REQUEST['rest-retypepassword'];

     if(checkUsers($email)){
        if($password=== $password_2){
            $reset = new DBManipulation();
            $reginfo = $reset->resetPasswordWithUUID($email,$password,$hiddenid);
            if(is_array($reginfo)){
                if(isset($reginfo['reg'])){
                    if(intval($reginfo['reg'])==1){
                        header('Content-Type: application/json');
                        echo json_encode(['register'=>'Password has been changed! Redirecting to log in page.']);
                    }else{
                        header('Content-Type: application/json');
                        echo json_encode(['register'=>'Password reset failed.']);
                    }
                }             
            }else{
                header('Content-Type: application/json');
                echo json_encode(['register'=>'Something went wrong. Please try again.']);
            }
        }else{
            header('Content-Type: application/json');
            echo json_encode(['register'=>'Passwords didn\'t match. Try again.']);
        }
     }else{
        header('Content-Type: application/json');
        echo json_encode(['register'=>'Not a valid email address! contact info@optergy.com']);
     }
}

//reset password init
if(isset($_REQUEST['resetpassword']) && !empty($_REQUEST['resetpassword'])){
     $email = $_REQUEST['resetpassword'];
     if(checkUsers($email)){
        $reset = new DBManipulation();
        $reginfo = $reset->resetPassword($email);
        if(is_array($reginfo)){
            if(isset($reginfo['reg_id']) && strlen($reginfo['reg_id'])>0){
                $sent = sendEmailForPasswordReset($reginfo['reg_id'],$email);
                if($sent){
                    header('Content-Type: application/json');
                    echo json_encode(['register'=>'Please check your email for password reset instruction!']);
                }else{
                    header('Content-Type: application/json');
                    echo json_encode(['register'=>'Something went wrong. Please try again.']);
                }                
            }
        }
     }else{
        header('Content-Type: application/json');
        echo json_encode(['register'=>'Not a valid email address! contact info@optergy.com']);
     }
}

/* user registation */
if(isset($_REQUEST['registeremail']) && isset($_REQUEST['password'])){
    if(!empty($_REQUEST['registeremail']) && !empty($_REQUEST['password'])){        
        $username = strval($_REQUEST['registeremail']);
        if(checkUsers($username)){
            $login = new DBManipulation();
            $password = strval($_REQUEST['password']);
            $reginfo = $login->register($username,$password);
            //echo json_encode(['logged'=>$reginfo]);
            header('Content-Type: application/json');
            if(is_array($reginfo)){
                if(isset($reginfo['error'])){
                    if(strlen($reginfo['error'])>2){
                        if($reginfo['error']=='duplicate'){
                            echo json_encode(['register'=>'User already registered!.','show'=>FALSE]);
                        }else{
                            echo json_encode(['register'=>'Cannot create a user account.','show'=>FALSE]);
                        }
                    }
                }
                else{
                    if(strlen($reginfo['reg_id'])>10){
                        sendUserNeedsToActivateEmail($username);
                        echo json_encode(['register'=>'Thank you for registering! We will review activate your account as soon as possible.','show'=>TRUE]);
                    }
                }
            }
        }else{
            header('Content-Type: application/json');
            echo json_encode(['register'=>'Not a valid email address! contact info@optergy.com']);
        }
    }
}
if(isset($_GET['start'])){
    header('Content-Type: application/json');
    echo json_encode(['process'=>'Backup conversion has been started...']);
}
if(isset($_GET['delete_file']) && !empty($_GET['delete_file'])){
    $delete_file = new DBManipulation();
    $filename = $_GET['delete_file'];
    header('Content-Type: application/json');
    $status = $delete_file->deleteConvertedFile($filename);
    echo $status;
}
if(isset($_GET['delete_info']) && !empty($_GET['delete_info'])){
    $delete_file = new DBManipulation();
    $filename = $_GET['delete_info'];
    header('Content-Type: application/json');
    $status = $delete_file->deleteConvertedInfo($filename);
    echo $status;
}
if(isset($_REQUEST['useractivate']) && !empty($_REQUEST['useractivate'])){
        header('Content-Type: application/json');
        $status_change = new DBManipulation();
        $val = intval($_REQUEST['status'])==1 ? 1 : 0;
        $status = $status_change->changeStatus($_REQUEST['useractivate'],$val);
        echo $status;
}

