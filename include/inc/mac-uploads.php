<?php

$currentDirectory = getcwd();
$uploadDirectory = "/uploads/";
$errors = [];
$fileExtensionsAllowed = ['csv'];
$fileName = $_FILES['file']['name'];
$fileSize = $_FILES['file']['size'];
$fileTmpName = $_FILES['file']['tmp_name'];
$fileType = $_FILES['file']['type'];
$fileExtention = explode('.', $fileName);
$fileExtension = strtolower(end($fileExtention));
$uploadPath = $currentDirectory . $uploadDirectory . basename($fileName);

if (isset($_POST['submit'])) {
    $result = array();
    if (!in_array($fileExtension, $fileExtensionsAllowed)) {
        $errors[] = "This file extension is not allowed. Please upload a CSV file";
    }

    if ($fileSize > 4000000) {
        $errors[] = "File exceeds maximum size (4MB)";
    }

    if (empty($errors)) {
        $didUpload = move_uploaded_file($fileTmpName, $uploadPath);

        if ($didUpload) {
            echo "The file " . basename($fileName) . " has been uploaded";
            $csvFile = file('./uploads/'.basename($fileName));
            $data = [];
            foreach ($csvFile as $line) {
                $data[] = str_getcsv($line);
            }
            $result['data'] = $data;
        } else {
            $result['errors'][0] = "An error occurred. Please contact the administrator.";
        }
    } else {
        foreach ($errors as $error) {
            $result['errors'][] = $error;
        }
    }
    return json_encode($result);
}
?>