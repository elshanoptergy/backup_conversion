#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

#PKG_FOLDER=$1
#gpg --passphrase test --batch --yes --pinentry-mode=loopback  --output /var/www/html/gnupg/$PKG_FOLDER.gpg --symmetric /var/www/html/gnupg/$PKG_FOLDER
#sleep=1
#exit 0
FILE_NAME=$1
DEB_PATH='/home/optergy/debs/'
USE_IP='optergy@192.168.3.232'
SSH_PASS='sshpass -p elshan'
BINPATH='/home/optergy/bins/'
KUNU='KunuOkkoma.jar'
UPDATES_STORE_PATH='/home/optergy/updates/'
_CHECK_VERSION () {
	$SSH_PASS ssh $USE_IP sudo java -jar $BINPATH$KUNU version '/home/optergy/updates/'$FILE_NAME
}
_CHECK_VERSION
