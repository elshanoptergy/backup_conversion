#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

USE_IP='optergy@192.168.3.232'
SSH_PASS='sshpass -p elshan'

FILE='/usr/local/Optergy/conf/global.properties'

FILE2='/usr/local/Optergy/conf/licencing.properties'

_COPY () {
    sudo $SSH_PASS ssh $USE_IP sed -i 's/setup_wizard_ran=false/setup_wizard_ran=true/g' $FILE
    echo 'global.properties edited'

    sudo $SSH_PASS ssh $USE_IP sed -i "'s/enabled=.*\(.\)/enabled=0/g'" $FILE2
    echo 'licencing.properties edited'
}

#_COPY
sudo $SSH_PASS ssh $USE_IP bash '/home/optergy/bins/cleanlic.sh'
#echo 'licencing.properties edited'
#echo 'global.properties edited'

exit 0

