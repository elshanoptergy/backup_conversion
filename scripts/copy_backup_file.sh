#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

USE_IP='optergy@192.168.3.232'
SSH_PASS='sshpass -p elshan'

UPDATES_STORE_PATH='/home/optergy/updates/'
HOME_MD5='/var/www/html/scripts/md5/'
UPLOAD_PATH='/var/www/html/jfileupload/server/php/files/'
UPLOAD_FOLDER=$1
UPLOAD_FILE=$2

_COPY () {

#sudo $SSH_PASS ssh $USE_IP sudo chmod -R 777 $UPDATES_STORE_PATH

#sudo $SSH_PASS ssh $USE_IP sudo chmod -R 777 /home/optergy/temp/

#sudo $SSH_PASS ssh $USE_IP sudo chmod -R 777 /home/optergy/bins/

sudo $SSH_PASS scp $UPLOAD_PATH$UPLOAD_FOLDER'/'$UPLOAD_FILE $USE_IP:$UPDATES_STORE_PATH
sleep 1
}

_COPY
exit 0

