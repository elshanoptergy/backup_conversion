#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

#------------------------------------------------#
# proton version as a parameter
FILE_NAME=$1
#------------------------------------------------#
# proton file path
DEB_PATH='/home/optergy/debs/'
#------------------------------------------------#
# Virtual machine path
USE_IP='optergy@192.168.3.232'
#------------------------------------------------#
# ssh password of above virtual machine
SSH_PASS='sshpass -p elshan'
#------------------------------------------------#

#print ok with format
INDEX=1
_OK () {
    sleep 1
    echo -e "\e[1m[$INDEX]:[OK]"
    ((INDEX=INDEX+1))
 }
 #printing with format
 _PRINT () {
     echo -e "\e[0m $INDEX:$1"
 }

#functions for removing old stuff
_REMOVE_OLD_PROTON () {

    #uninstall optergy
    _PRINT "Removing exisiting installation of Proton"
    $SSH_PASS ssh $USE_IP sudo dpkg -P proton
    _OK

    #remove optergy files
    _PRINT "Removing Proton files.."
    $SSH_PASS ssh $USE_IP sudo rm -rf /usr/local/tomcat /usr/local/Optergy
    _OK

    #Stopping PSQL
    _PRINT "Stopping postgresql"
    $SSH_PASS ssh $USE_IP sudo service postgresql restart
    _OK

    #Deleting dbs
    _PRINT "Deleting optergy DB"
    $SSH_PASS ssh $USE_IP dropdb optergy
    _OK

    _PRINT "Deleting countlogs DB"
    $SSH_PASS ssh $USE_IP dropdb countlogs
    _OK

    _PRINT "Deleting issues DB"
    $SSH_PASS ssh $USE_IP dropdb issues
    _OK

    _PRINT "Deleting trendlogs DB"
    $SSH_PASS ssh $USE_IP dropdb trendlogs
    _OK

    _PRINT "Deleting energlylogs DB"
    $SSH_PASS ssh $USE_IP dropdb energylogs
    _OK
}

_INSTALL_OLD_PROTON () {

    _PRINT "Installing Proton"
    $SSH_PASS ssh $USE_IP sudo dpkg -i --force-confnew $DEB_PATH$FILE_NAME
    _OK
}

#remove old files on remove machine
#_PRINT "Removing : $FILE_NAME"
#$SSH_PASS ssh $USE_IP rm -f /home/optergy/temp/$FILE_NAME
#_OK

#copying new file
#                _PRINT "Copying Remotely : $FILE_NAME"
#                $SSH_PASS scp $FILE_NAME $USE_IP:/home/optergy/debs
#                _OK#
#
#
_PRINT "Starting debian convert..."
if $SSH_PASS ssh $USE_IP stat $DEB_PATH$FILE_NAME \> /dev/null 2\>\&1
            then
                _PRINT "Remote deb exsist : $FILE_NAME"
                _OK
            else
                _PRINT "Debian file $DEB_PATH$FILE_NAME could not found on $USE_IP"
                exit 0
fi

#remove old md5
_PRINT "Removing MD5 $FILE_NAME.md5.txt"
rm -f $FILE_NAME.md5.txt
_OK

#then copy
_PRINT "Creting md5: $FILE_NAME.md5.txt"
$SSH_PASS ssh $USE_IP md5sum $DEB_PATH$FILE_NAME > $FILE_NAME.md5.txt
_OK

#remove old proton content
_REMOVE_OLD_PROTON

#install old proton version
_INSTALL_OLD_PROTON

exit 0
