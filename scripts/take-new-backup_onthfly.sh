#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

USE_IP='optergy@192.168.3.232'
SSH_PASS='sshpass -p elshan'

UPDATES_STORE_PATH='/home/optergy/updates/'
TYPE=$1
BACKUP_NAME=$2
FOLDER=$3
IS_NEW=$4

ROOT='/var/www/html/jfileupload/server/php/files/'
_COPY () {

sleep 1
$SSH_PASS ssh $USE_IP rm -f /home/optergy/temp/*.gpg
sleep 1
$SSH_PASS ssh $USE_IP rm -f /home/optergy/temp/*.backup
sleep 1

FINAL_BACKUP_NAME=""

DR=$ROOT$FOLDER'_encrypts/'
mkdir -p $DR

if [ $IS_NEW -eq 0 ] ; then
    echo 'old backup copied to vm '$BACKUP_NAME'backup'
    echo $SSH_PASS scp $ROOT$FOLDER'/'$BACKUP_NAME'backup' $USE_IP:/home/optergy/temp/
    $SSH_PASS scp $ROOT$FOLDER'/'$BACKUP_NAME'backup' $USE_IP:/home/optergy/temp/
else
    echo 'new backup copied backup folder'
    sudo cp $ROOT$FOLDER'/'$BACKUP_NAME $DR
    #echo $SSH_PASS scp $ROOT$FOLDER'/'$BACKUP_NAME'gpg' $USE_IP:/home/optergy/temp/
    #$SSH_PASS scp $ROOT$FOLDER'/'$BACKUP_NAME'gpg' $USE_IP:/home/optergy/temp/
fi
sleep 1

if [ $IS_NEW -eq 0 ] ; then
    echo 'old converted ' $BACKUP_NAME'backup :TYPE ' $TYPE
    echo $SSH_PASS ssh $USE_IP 'java -jar /home/optergy/bins/KunuOkkoma.jar ' $TYPE  /home/optergy/temp/$BACKUP_NAME'backup'
    $SSH_PASS ssh $USE_IP 'java -jar /home/optergy/bins/KunuOkkoma.jar ' $TYPE  /home/optergy/temp/$BACKUP_NAME'backup'
else
    echo 'nothing to do with encryption for new backups'
    #echo $SSH_PASS ssh $USE_IP 'java -jar /home/optergy/bins/KunuOkkoma.jar ' $TYPE  /home/optergy/temp/$BACKUP_NAME
    #$SSH_PASS ssh $USE_IP 'java -jar /home/optergy/bins/KunuOkkoma.jar ' $TYPE  /home/optergy/temp/$BACKUP_NAME
fi

sleep 1





if [ $IS_NEW -eq 0 ] ; then
    echo $SSH_PASS sudo scp $USE_IP':/home/optergy/temp/'$BACKUP_NAME'gpg' $DR
    $SSH_PASS sudo scp $USE_IP':/home/optergy/temp/'$BACKUP_NAME'gpg' $DR
else
    echo 'No need to copy anything with new backup'
    #echo $SSH_PASS sudo scp $USE_IP':/home/optergy/temp/'$BACKUP_NAME $DR
    #$SSH_PASS sudo scp $USE_IP':/home/optergy/temp/'$BACKUP_NAME $DR
fi

sleep 1
echo 'OK'

}

_COPY

exit 0

