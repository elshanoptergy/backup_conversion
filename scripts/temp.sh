#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

USE_IP='optergy@192.168.3.232'
SSH_PASS='sshpass -p elshan'

UPDATES_STORE_PATH='/home/optergy/updates/'
TYPE=$1
BACKUP_NAME=$2
FOLDER=$3
_COPY () {

#sudo $SSH_PASS ssh $USE_IP cp $UPDATES_STORE_PATH$UPLOAD_FILE '/usr/local/Optergy/Backup/'
#sleep 2
sudo $SSH_PASS ssh $USE_IP 'rm -f /usr/local/Optergy/Backup/*.*'

sudo $SSH_PASS ssh $USE_IP 'mkdir -p /usr/local/Optergy/Backup/Auto/'
sleep 1
sudo $SSH_PASS ssh $USE_IP 'java -jar /home/optergy/bins/KunuOkkoma.jar ' $TYPE  $BACKUP_NAME
sleep 1
sudo $SSH_PASS ssh $USE_IP 'rm -f /usr/local/Optergy/Backup/*.backup'
sleep 1
sudo $SSH_PASS ssh $USE_IP 'ls /usr/local/Optergy/Backup/'

DR='/var/www/html/jfileupload/server/php/files/'$FOLDER'_encrypts/'
mkdir -p $DR

$SSH_PASS sudo scp $USE_IP:'/usr/local/Optergy/Backup/'$BACKUP_NAME-part1.backup.gpg $DR
#sudo scp optergy@192.168.3.232:/usr/local/Optergy/Backup/Trend-08887736236-part1.backup.gpg /var/www/html/jfileupload/server/php/files/e1725994612221db3808739b5465054e_encrypts

}

_COPY

exit 0

