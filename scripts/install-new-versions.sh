#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

USE_IP='optergy@192.168.3.232'
SSH_PASS='sshpass -p elshan'

DEB_PATH='/home/optergy/debs/'
FILE_NAME=$1

_COPY () {

#sudo $SSH_PASS ssh $USE_IP sudo chmod -R 777 $UPDATES_STORE_PATH

#sudo $SSH_PASS ssh $USE_IP sudo chmod -R 777 /home/optergy/temp/

#sudo $SSH_PASS ssh $USE_IP sudo chmod -R 777 /home/optergy/bins/

$SSH_PASS ssh $USE_IP sudo dpkg -i --force-confnew $DEB_PATH$FILE_NAME

}

_COPY
exit 0

