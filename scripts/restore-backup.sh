#!/bin/bash

#------------------------------------------------#
#------ VERSION 0.1 BY ELSHAN | 04/2020 ---------#
#------------------------------------------------#
# Better to change the deb path and the username #
# password according to your VM - by Elshan      #
#------------------------------------------------#

USE_IP='optergy@192.168.3.232'
SSH_PASS='sshpass -p elshan'

UPDATES_STORE_PATH='/home/optergy/updates/'
UPLOAD_FILE=$1

_COPY () {

sudo $SSH_PASS ssh $USE_IP cp $UPDATES_STORE_PATH$UPLOAD_FILE '/usr/local/Optergy/Backup/'
sleep 2
sudo $SSH_PASS ssh $USE_IP 'java -jar /home/optergy/bins/KunuOkkoma.jar restore ' $UPLOAD_FILE

}

_COPY

exit 0

