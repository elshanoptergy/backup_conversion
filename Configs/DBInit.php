<?php
/*
error_reporting(E_ALL);

try{
        $conn = new PDO('mysql:host=localhost;dbname=encbackups', 'root', 'elshan');
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$stmt = "select loginkey from users;";
        try {
            //$con = $this->connect();
            $stmt = $conn->prepare($stmt);
            //$stmt->bindValue(':id', $id);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_COLUMN);
                print $row;
            }
        } catch (Exception $ex) {
            print $ex->getMessage();
        }
//            var_dump($conn);
        }catch(PDOException $ex){
            print $ex->getMessage();
            die();
        }
*/
/*
 * Database connection class.
 * Developer | Elshan Kodikara 2019
 */
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
class DBInit {
    protected static function connect() {
        try{
            $conn = new PDO('mysql:host=localhost;dbname=encbackups', 'root', 'elshan');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $conn;
        }catch(PDOException $ex){
 	//	print $ex->getMessage();	
            print $ex->getCode();
            die();
        }
    }
}
/*
	try{
            $conn = new PDO('mysql:host=localhost;dbname=encbackups', 'root', '');
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            var_dump($conn);
        }catch(PDOException $ex){
            print $ex->getCode();
            die();
        }
*/

?>
