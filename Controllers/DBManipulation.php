<?php
@session_start();
$ROOT_URL = $_SERVER["DOCUMENT_ROOT"] . '/';
include $ROOT_URL . 'Configs/DBInit.php';

class DBManipulation extends DBInit {
    public function getAllTestRigsForEdit() {
        $stmt = "select * from testrigs order by rig_code";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            if ($stmt->execute()) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    echo '<tr><td>' . $row['id'] . '</td>';
                    echo '<td>' . $row['rig_code'] . '</td>';
                    echo '<td>' . $row['description'] . '</td>';
                    echo '</tr>';
                }
            }
        } catch (PDOException $ex) {
            
        }
    }
    public function str_lreplace_($search, $replace, $subject)
    {
        $pos = strrpos($subject, $search);
        if($pos !== false)
        {
            $subject = substr_replace($subject, $replace, $pos, strlen($search));
        }
        return $subject;
    }
   public function getUploadedBackups($id){
        $folder = $id .'_encrypts/';
        $fulldir = '/jfileupload/server/php/files/'.$folder;
       $stmt = "select * from files where userid=:userid and (fsendfirst = 0 or fsendfirst = -1 or fsendfirst = 1 or fsendfirst = -4) order by id";
       try{
           $con = $this->connect();
           $stmt = $con->prepare($stmt);
           $stmt->bindValue(':userid', $id);
           if($stmt->execute()){
               $i = 1;
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $filename = $row['filename'];                
                $filename = $this->str_lreplace_('.backup','.gpg',$filename);
                $downloadLink = $fulldir.$filename;
                echo '<tr id="row-'.$i.'">';
                echo '<td>' . $i . '</td>';
                echo '<td>' . $row['filename'] . '</td>';
                echo '<td>' . $row['version'] . '</td>';

                if($row['fsendfirst']==-1){
                    echo '<td><span class="label label-danger" data-toggle="tooltip" data-placement="top" title="File has been deleted before start the conversion!" alt="File deleted before the convert">N/C & Deleted</span></td>';
                }else if($row['fsendfirst']==1){
                    echo '<td><span class="label label-primary">Completed</span></td>';
                }
                else if($row['fsendfirst']==-4){
                    echo '<td><span class="label label-primary" data-toggle="tooltip" data-placement="top" title="This version is not compatable to convert!" >Version Issue</span></td>';
                }
                else if($row['fsendfirst']==0){
                    echo '<td><span class="label label-warning">Converting</span> <div class="spinner-grow spinner-border-sm text-info" role="status"><span class="sr-only">Converting...</span></div></td>';
                }else if($row['fsendfirst']==NULL || empty($row['fsendfirst'])){
                    echo '<td><span class="label label-warning">Queueing</span></td>';
                }
                else{}

                echo '<td>' . $row['uploaded_date'] . '</td>';
                echo '<td>' . $row['finished_data'] . '</td>';

                if($row['fsendfirst']==-1 || $row['fsendfirst']==-2){
                    echo '<td><a href="#" class="btn btn-disabled">Unavailable</button></td>';
                }else if($row['fsendfirst']==0){
                    echo '<td><a class="btn btn-light" alt="download" btn-disabled> Download </a></td>';
                }
                else if($row['fsendfirst']==1){
                    echo '<td><a class="btn btn-primary" href="'.$downloadLink.'" alt="download"> Download </a></td>';
                }else{
                   echo '<td></td>';
                }

                if($row['fsendfirst']==1){
                    echo '<td><a class="btn btn-danger delete_converted" data="'.$filename.'" href="#" alt="download"><i class="glyphicon glyphicon-trash"></i> Delete File </a></td>';
                }else if($row['fsendfirst']==-1 || $row['fsendfirst']==1 || $row['fsendfirst']==-4){                
                    echo '<td><button class="btn btn-info delete_non_related_info" data="'.$row['id'].'" href="#" alt="download"><i class="glyphicon glyphicon-trash"></i> Remove </button></td>';
                }else{
                    echo '<td></td>';
                }
                echo '</tr>';
                $i++;
            }
           }
       } catch (Exception $ex){
           return 0;
       }
   }
    public function getLastLoginHash($id) {
        $stmt = "select loginkey from users where id=:id";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':id', $id);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_COLUMN);
                return $row;
            }
        } catch (Exception $ex) {
            return 0;
        }
    }

    public function register($username, $password) {
        $response = array();
        $lastid =0;
        $stmt = "insert into users(name,password,level,loginkey) values(:username,:password,2,:uuid)";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':username', $username);
            $stmt->bindValue(':password', md5($password));
            $stmt->bindValue(':uuid', md5(date('y-m-d H:m:s')));
            $con->beginTransaction();
            if ($stmt->execute()) {
                $lastid = $con->lastInsertId();
            }
            $con->commit();
            $response['reg_id'] = $this->getLastLoginHash($lastid);
        } catch (Exception $ex) {
            if ($ex->getCode() == 23000) {
                $response['error'] = 'duplicate';
            } else {
                $response['error'] = $ex->getMessage();
            }
            $response['reg_id'] ='';
        }
        return $response;
    }
    public function resetPassword($email){
        $response = array();
        $stmt = "select id,level,name,loginkey from users where name=:username";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':username', $email);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                $response['reg_id']= $row['loginkey'];
            }
        } catch (Exception $ex) {
            $response['reg_id']="";
        }
        return $response;
    }
    public function resetPasswordWithUUID($email,$password,$hiddenid){
        $response = array();
        $stmt = "update users set password=:password,lastupdate_date=:lastupdate_date where name=:username and loginkey=:loginkey and factive=1";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':loginkey', $hiddenid);
            $stmt->bindValue(':username', $email);
            $stmt->bindValue(':password', md5($password));
            $stmt->bindValue(':lastupdate_date', date("Y-m-d H:i:s"));
            if($stmt->execute()){
                $count = $stmt->rowCount();
                $response['reg']=$count;
                $this->_log_("PASSWD_RESET",$email);
            }
        } catch (Exception $ex) {
            $response['reg']=0;
            $this->_log_("PASSWD_RESET",$email . " ERR" . $ex->getMessage());
        }
        return $response;
    }
    public function checkLogin($username, $password) {
        $stmt = "select id,level,name,loginkey,factive from users where name=:username and password=:password";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':username', $username);
            $stmt->bindValue(':password', md5($password));
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row;
            }
        } catch (Exception $ex) {
            $this->_log_("LOGIN","ERR " .$ex);
            return false;
        }
    }
    //hide from the view page -2 [mark as file deleted]
    public function deleteConvertedFile($filename){
        $user_key = isset($_SESSION['loginkey']) ? $_SESSION['loginkey'] : 0;
        $stmt = "update files set fsendfirst=-2 where filename like :filename and userid=:userid";
        $filename = str_replace('.backup', "", $filename);
        $filename = str_replace('.gpg', "", $filename);
        try {

            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':filename', $filename.'%');
            $stmt->bindValue(':userid', $user_key);
            $val = $stmt->execute();
            $count = $stmt->rowCount();
            if (intval($count)>0) {
                $this->_log_("DELETTING CONVERTED FILES",$filename . " DONE");
                return json_encode(array('data'=>true));
            }
        } catch (Exception $ex) {
            $this->_log_("DELETE FAIL",$filename . " ERR" . $ex->getMessage());
            return json_encode(array('data'=>false));
        }
    }
    //-9 mean hide from the view page [mark as info deleted]
    public function deleteConvertedInfo($id){
        $user_key = isset($_SESSION['loginkey']) ? $_SESSION['loginkey'] : 0;
        $stmt = "update files set fsendfirst=-9 where id=:id and userid=:userid";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':id', $id);
            $stmt->bindValue(':userid', $user_key);
            $val = $stmt->execute();
            if ($val) {
                return json_encode(array('data'=>$val));
            }
        } catch (Exception $ex) {
            return json_encode(array('data'=>false));
        }
    }
    public function checkUserHasUploads(){
         $user_key = isset($_SESSION['loginkey']) ? $_SESSION['loginkey'] : 0;
         try {
            $stmt = "select id,userid,filename from files where fsendfirst = 1 and filename IS NOT NULL and userid=:userid order by id desc limit 0,1";
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':userid', $user_key);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row ? true : false;
            }
            return false;
        } catch (Exception $ex) {
            $this->_log_("checkUserHasUploads","ERROR " .$ex);
            return false;
        }
    }
    private function url_origin( $s, $use_forwarded_host = false )
    {
        $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
        $sp       = strtolower( $s['SERVER_PROTOCOL'] );
        $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
        $port     = $s['SERVER_PORT'];
        $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
        $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
        $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
        return $protocol . '://' . $host;
    }

    private function full_url( $s, $use_forwarded_host = false )
    {
        return $this->url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
    }

    public function _log_($logtype="",$description="",$url=""){
        $user_key = isset($_SESSION['loginkey']) ? $_SESSION['loginkey'] : 0;
        
        $ip_x_forward="";
        $ip_client_ip="";
        $ip="";

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip_client_ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip_x_forward = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        if(!empty($_SERVER['REMOTE_ADDR'])){
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        
        $url = $this->full_url($_SERVER);

        try{
            $stmt = "insert into userlog(user_id,logtype,description,ip,ip_client_ip,ip_x_forward,url) values(:user_id,:logtype,:description,:ip,:ip_client_ip,:ip_x_forward,:url)";
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':user_id', $user_key);
            $stmt->bindValue(':logtype', $logtype);
            $stmt->bindValue(':description', $description);
            $stmt->bindValue(':ip', $ip);
            $stmt->bindValue(':ip_client_ip',$ip_client_ip);
            $stmt->bindValue(':ip_x_forward',$ip_x_forward);
            $stmt->bindValue(':url', $url);
            $stmt->execute();
        }catch(Exception $ex){
             //$this->_log_("getUserStatus","ERROR " .$ex);
        }
    }

    public function getUserStatus(){
        try{
            $stmt = "select * from users where level not in (0,1) order by factive desc,added_date;";
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            if($stmt->execute()){
               $i = 1;
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $name = $row['name'];
                    $loginkey = $row['loginkey'];
                    $added_date = $row['added_date'];
                    $lastupdate_date = $row['lastupdate_date'];
                    $factive = $row['factive'];
                    $i++;     
                    echo '<tr id="row-'.$i.'">';
                    echo '<td data='.$factive .' >' . $i . '</td>';  
                    echo '<td>' . $name . '</td>';
                    echo '<td>' . $loginkey . '</td>'; 
                    echo '<td>' . $added_date . '</td>'; 
                    echo '<td>' . $lastupdate_date . '</td>'; 
                    if( empty($factive) || $factive == NULL || intval($factive) == 0){
                        echo '<td><button onclick="manipulateUsers(this);return false;" data="'.$loginkey.'" class="btn btn-primary" nodetype="1">Activate</button></td>';     
                    }else{
                        echo '<td><button onclick="manipulateUsers(this);return false;" data="'.$loginkey.'" class="btn btn-danger" nodetype="0">Deactivate</button></td>'; 
                    }
                    
                    echo '</tr>';
                    /*
                    <th>#</th>
                            <th>User Name</th>
                            <th>Created Date</th>
                            <th>Updated Date</th>
                            <th>Waiting Period</th>
                            <th>Status</th>
                            <th>Option</th>
                    */
                }
            }
        }catch(Exception $ex){
            $this->_log_("getUserStatus","ERROR " .$ex);
        }
    }
    /*
     public function checkLogin($username, $password) {
        $stmt = "select id,level,name,loginkey,factive from users where name=:username and password=:password";
        try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':username', $username);
            $stmt->bindValue(':password', md5($password));
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row;
            }
        } catch (Exception $ex) {
            $this->_log_("LOGIN","ERR " .$ex);
            return false;
        }
    }*/
    private function getEmailFromKey($key='')
    {
       $stmt = "select name from users where loginkey=:key";
       try {
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':key', $key);
            if ($stmt->execute()) {
                $row = $stmt->fetch(PDO::FETCH_ASSOC);
                return $row['name'];
            }
        } catch (Exception $ex) {
            $this->_log_("getEmailFromKey","ERR " .$ex);
            return '';
        }
    }
    public function changeStatus($key,$status){
        try{
            $stmt = "update users set factive=:status where loginkey=:key";
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            $stmt->bindValue(':status', $status);
            $stmt->bindValue(':key', $key);
            $result = $stmt->execute();
            if(intval($result)>0){
                $email = $this->getEmailFromKey($key);
                $this->sendAccountActivatedEmail($email);
            }
            return $result;
        }catch(Exception $ex){
            $this->_log_("STATUS_CHANGE","ERROR " .$ex);
            return 0;
        }
    }
    private function sendAccountActivatedEmail($email){
       $to = $email;
       $subject = "Optergy Backup Conversion - Reset Password";
       
       $message = "<p><b>Your account has been activated.</p>";
       $message .= "<p>Pleaes use this url to log in.<a href='https://backuptool.optergy.com:8448/'>https://backuptool.optergy.com:8448/</a></p>";
       $header = "From:tools@optergy.com \r\n";
       $header .= "MIME-Version: 1.0\r\n";
       $header .= "Content-type: text/html\r\n";   
       $retval = mail ($to,$subject,$message,$header);   
       return $retval;
    }
    public function isValidAdmin(){
        try{
            if(isset($_SESSION['loginkey'])){
                $stmt = "select id from users where (level BETWEEN 0 and 1) and loginkey=:loginkey";
                $con = $this->connect();
                $stmt = $con->prepare($stmt);
                $stmt->bindValue(':loginkey', $_SESSION['loginkey']);
                if ($stmt->execute()) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    if(intval($row['id'])>0){
                        return true;
                    }
                }
            }
            return false;
        }catch(Exception $ex){
            $this->_log_("GET_VALID_USER_LEVEL1","ERROR " .$ex);
            return false;
        }
    }
    public function getValidAdminEmails(){
        try{
            $stmt = "select * from users where (level BETWEEN 0 and 1)";
            $con = $this->connect();
            $stmt = $con->prepare($stmt);
            if ($stmt->execute()) {
                $row = $stmt->fetchAll(PDO::FETCH_ASSOC);
                return $row;
            }
            return null;
        }catch(Exception $ex){
            $this->_log_("Getting Admins for sending an email","ERROR " .$ex);
            return null;
        }
    }
    //deleteConvertedInfo
}
