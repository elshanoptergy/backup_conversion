<?php

class Helpers {
    private static $filename = ROOT."/../864API/EmLibs/Controllers/configs.ini";
    public static function writeIni($array,$file) {
        $res = array();
        foreach ($array as $key => $val) {
            if (is_array($val)) {
                $res[] = "[$key]";
                foreach ($val as $skey => $sval) {
                    $res[] = "$skey = " . (is_numeric($sval) ? $sval : '"' . $sval . '"');
                }
            } else {
                $res[] = "$key = " . (is_numeric($val) ? $val : '"' . $val . '"');
            }
        }
        self::safefilerewrite(implode("\r\n", $res),$file);
    }

    private function safefilerewrite($dataToSave,$file) {
        if ($fp = fopen($file, 'w')) {
            $startTime = microtime(TRUE);
            do {
                $canWrite = flock($fp, LOCK_EX);
                // If lock not obtained sleep for 0 - 100 milliseconds, to avoid collision and CPU load
                if (!$canWrite) {
                    usleep(round(rand(0, 100) * 1000));
                }
            } while ((!$canWrite)and ( (microtime(TRUE) - $startTime) < 5));

            //file was locked so now we can store information
            if ($canWrite) {
                fwrite($fp, $dataToSave);
                flock($fp, LOCK_UN);
            }
            fclose($fp);
        }
    }
    public static function ReadRigCode(){
        return self::ReadIni('rigid');
    }
    private static function ReadIni($tag){
        $ini_array = parse_ini_file(self::$filename);
        return $ini_array[$tag];
    }

}
